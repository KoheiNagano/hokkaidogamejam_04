﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ActionMgr : MonoBehaviour{

    //アクションを格納
    private Dictionary<string, System.Action> m_ActionDics = new Dictionary<string, System.Action>();

    private Animator m_Animator;

    private bool m_IsOnGround;

    [SerializeField]
    private GameObject m_AttackCollider;
    [SerializeField]
    private GameObject m_DefenceCollider;

    [SerializeField]
    private List<AudioClip> m_Ses = new List<AudioClip>();

    private AudioSource m_AudioSource;

    // Use this for initialization
    void Start()
    {
        m_Animator = GetComponent<Animator>();
        m_AudioSource = GetComponent<AudioSource>();

        m_ActionDics.Add("A", () => Jump());
        m_ActionDics.Add("B", () => Attack());
        m_ActionDics.Add("X", () => Defence());
        m_ActionDics.Add("Y", () => Other());
    }

    // Update is called once per frame
    void Update()
    {
        if (FlowManager.gameFlow != GameFlow.ACTION) return;

        ActionUpdate();
    }

    private void ActionUpdate()
    {
        //再生中のアニメーションが走るでない、または、地面についていない時は実行しない
        if (!m_Animator.GetCurrentAnimatorStateInfo(0).IsName("run") || !m_IsOnGround) return;

        //Aボタンのアクション
        if (Input.GetButtonDown("A") || Input.GetKeyDown(KeyCode.A))
        {
            m_ActionDics["A"]();
        }
        //Bボタンのアクション
        if (Input.GetButtonDown("B") || Input.GetKeyDown(KeyCode.B))
        {
            m_ActionDics["B"]();
        }
        //Xボタンのアクション
        if (Input.GetButtonDown("X") || Input.GetKeyDown(KeyCode.X))
        {
            m_ActionDics["X"]();
        }
        //Yボタンのアクション
        if (Input.GetButtonDown("Y") || Input.GetKeyDown(KeyCode.Y))
        {
            m_ActionDics["Y"]();
        }
    }

    //ジャンプの処理
    private void Jump()
    {
        m_Animator.SetTrigger("jumpTrigger");
        GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 1) * 350);

        m_AudioSource.clip = m_Ses[0];
        m_AudioSource.Play();

    }

    //攻撃の処理
    private void Attack()
    {
        m_Animator.SetTrigger("attackTrigger");

        Instantiate(m_AttackCollider, transform.position + new Vector3(1, 0), Quaternion.identity);

        m_AudioSource.clip = m_Ses[1];
        m_AudioSource.Play();

    }

    //防御の処理
    private void Defence()
    {
        m_Animator.SetTrigger("defenceTrigger");

        Instantiate(m_DefenceCollider, transform.position + new Vector3(1, 0), Quaternion.identity);

        m_AudioSource.clip = m_Ses[2];
        m_AudioSource.Play();

    }

    //その他
    private void Other()
    {
        //m_Animator.SetTrigger("jumpTrigger");
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.transform.tag == "Floor")
        {
            m_IsOnGround = true;
        }

        if (collision.transform.tag == "Enemy")
        {
            Debug.Log("dead");

            State.isDead = true;
        }


        if (collision.transform.tag == "Boss")
        {
            Debug.Log("dead");
            GetComponent<BoxCollider2D>().enabled = false;
            AreaController.areaSpeed = 0;
            State.isDead = true;
        }
    }

    public void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.transform.tag == "Floor")
        {
            m_IsOnGround = false;
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "Enemy")
        {
            Debug.Log("dead");
            State.isDead = true;
        }

        if (collision.transform.tag == "Boss")
        {
            Debug.Log("dead");
            GetComponent<BoxCollider2D>().enabled = false;
            AreaController.areaSpeed = 0;
            State.isDead = true;
        }
    }    
}

    
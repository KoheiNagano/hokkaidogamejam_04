﻿using UnityEngine;
using System.Collections;

using HondaUtil;

public class DeleteCollider : MonoBehaviour {

    Timer timer;

	// Use this for initialization
	void Start () {
        timer = new Timer(1);
	}
	
	// Update is called once per frame
	void Update () {
        timer.Update();

        if (timer.IsEnd())
        {
            Destroy(this.gameObject);
        }
	}
}

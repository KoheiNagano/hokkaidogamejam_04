﻿using UnityEngine;
using System.Collections;

public enum Action{
    Jump       = 0, //ジャンプ
    Attack     = 1, //攻撃
    Defence   = 2, //回避(上)
    AroundDown = 3, //回避(下)
    Count      = 4  //アクションの数
}

﻿using UnityEngine;
using System.Collections;

public class Boss : MonoBehaviour {

    //現在の状態
    [SerializeField]
    private BossState m_State;

    //ボスの初期位置
    private readonly Vector2 FIRST_POS = new Vector2(-8.0f, 0);
    //上下運動の幅
    private readonly float SHAKE_RANGE = 0.1f;

    private float angle = 0.0f;

    // Use this for initialization
    void Start()   {    }

    // Update is called once per frame
    void Update()
    {
        switch (m_State)
        {
            case BossState.Chase: Chasing();
                break;
            case BossState.Attack: Attacking();
                break;
            default: break;
        }
    }

    //追跡時の処理
    private void Chasing()
    {
        angle += Time.deltaTime;
        //sinカーブで上下運動
        Vector2 position = FIRST_POS + new Vector2(0, Mathf.Sin(angle * Mathf.Rad2Deg)) * SHAKE_RANGE;
        //座標に代入
        transform.position = position;
    }

    private void Attacking()
    {

    }
}

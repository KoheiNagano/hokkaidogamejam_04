﻿using UnityEngine;
using System.Collections;

public class Hunter : MonoBehaviour {

    [SerializeField]
    private AudioClip m_AudioClip;

    private AudioSource m_AudioSource;

    [SerializeField]
    private Animator m_Animator;

    [SerializeField]
    private Sprite m_DeadSprite;

    void Start()
    {
        m_AudioSource = GetComponent<AudioSource>();
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "AttackCollider")
        {
            m_AudioSource.clip = m_AudioClip;
            m_AudioSource.Play();

            Debug.Log("attack");

            StartCoroutine(Dead());
        }
    }

    private IEnumerator Dead()
    {
        GetComponent<SpriteRenderer>().sprite = m_DeadSprite;
        m_Animator.enabled = true;

        yield return new WaitForSeconds(0.6f);

        Destroy(this.gameObject);

        yield break;
    }

}

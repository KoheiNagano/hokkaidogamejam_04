﻿using UnityEngine;
using System.Collections;

public class Arrow : MonoBehaviour
{
    public float speed = 0.3f;

    [SerializeField]
    private Animator m_Animator;

    // Update is called once per frame
    void Update()
    {
        speed = AreaController.areaSpeed * 1.5f;

        if (m_Animator.enabled) transform.position += new Vector3(speed, 0.0f, 0.0f);
        else transform.position += new Vector3(-speed, 0.0f, 0.0f);
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "DefenceCollider")
        {
            Debug.Log("defence");

            StartCoroutine(Dead());
        }
    }

    private IEnumerator Dead()
    {
        m_Animator.enabled = true;

        yield return new WaitForSeconds(0.6f);

        Destroy(this.gameObject);

        yield break;
    }

}

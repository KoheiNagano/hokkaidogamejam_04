﻿using UnityEngine;
using System.Collections;

public class WallScroll : MonoBehaviour
{

    private float speed = AreaController.areaSpeed;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        speed = AreaController.areaSpeed;
        transform.position += new Vector3(-speed, 0.0f, 0.0f);
        if (transform.position.x < -9.8f)
        {
            transform.position = new Vector3(30.84f, 0.5f, 0.0f);
        }
    }
}

﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System.Linq;

public class AreaController : MonoBehaviour
{
    public int areaMax = 10;
    public static int areaNum = 0;
    public float biewAreaSpeed = 0.5f;
    public static float areaSpeed;
    public GameObject defaultTarget;
    private GameObject nextTarget;
    public List<GameObject> gimmicks;
    public List<string> gimmickNames;
    private Dictionary<string, GameObject> gimmickDictionary = new Dictionary<string, GameObject>();
    private bool isRun = false;

    public void Awake()
    {
        areaSpeed = biewAreaSpeed;
    }

    // Use this for initialization
    void Start()
    {
        nextTarget = defaultTarget;
        for (int i = 0; i < gimmicks.Count(); i++)
        {
            gimmickDictionary.Add(gimmickNames[i], gimmicks[i]);
        }
        areaNum = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isRun) return;
        if (areaNum < areaMax)
        {
            Instantiate(nextTarget);
            if (nextTarget != defaultTarget)
            {
                nextTarget = defaultTarget;
            }
        }
    }

    public void SetNextArea(string gimmick)
    {
        nextTarget = gimmickDictionary[gimmick];
    }


    public void RunArea()
    {
        isRun = true;
    }
}

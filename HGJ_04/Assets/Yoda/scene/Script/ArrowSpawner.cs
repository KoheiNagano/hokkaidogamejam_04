﻿using UnityEngine;
using System.Collections;

public class ArrowSpawner : MonoBehaviour
{
    public GameObject arrow;
    // Use this for initialization
    void Start()
    {
        Instantiate(arrow);
    }
}

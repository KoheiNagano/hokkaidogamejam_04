﻿using UnityEngine;
using System.Collections;

public class Area : MonoBehaviour
{
    private float speed;

    // Use this for initialization
    void Start()
    {
        AreaController.areaNum++;
        speed = AreaController.areaSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        speed = AreaController.areaSpeed;
        transform.position += new Vector3(-speed, 0f, 0f);
        if (transform.position.x <= -11)
        {
            AreaController.areaNum--;
            Destroy(gameObject);
        }
    }
}

﻿using UnityEngine;
using System.Collections;

using HondaUtil;

public class Player : MonoBehaviour {
    [SerializeField]
    int life;

    Range<int> lifeRange;

    [SerializeField]
    float deadLine = -10;

    MovetoTarget move;
    //Range<int> lifeRange;

    void Start()
    {
        gameObject.SetActive(false);
        lifeRange = new Range<int>(0, 5);
        move = GetComponent<MovetoTarget>();
    }

    void Update()
    {
        Debug.Log("life = " + life);
        DeadCheck();

        if (FlowManager.gameFlow != GameFlow.COMMAND) return;
        life = lifeRange.Clamp(life);
        if (!move.IsEnabled())
        {
            if (lifeRange.IsMax(life))
                move.SetParameters(transform.position, new Vector3(0, -2.347334f, 0), 0.5f);
            else
                move.SetParameters(transform.position, new Vector3(deadLine / 5 * (5 - life), -2.347334f), 0.5f);

            move.MoveStart();
        }
    }

    private void DeadCheck()
    {
        if (!State.isDead)
        {
            State.isDead = life <= 0;
        }
    }
    
    /// <summary>
    /// 距離が１回復
    /// </summary>
    public void Recovery()
    {
        life += 1;
    }
 
    /// <summary>
    /// 距離が１縮む
    /// </summary>
    public void Damage()
    {
        life -= 1;
    }

    void OnCollsionEnter()
    {
        State.isDead = true;
    }
}

﻿using UnityEngine;
using System.Collections;

public enum ActionState
{
    RUN,
    DAMAGE,
    JUMP,
}

public class State : MonoBehaviour {
    
    //現在の行動状況・ムーブ管理で使用
    public static ActionState actionState;
    //死んだかどうか
    public static bool isDead;

	// Use this for initialization
	void Start () {
        actionState = ActionState.RUN;
        isDead = false;
    }
	
	// Update is called once per frame
	void Update () {
        DeadCheck();
	}

    void DeadCheck()
    {
        if (isDead)
        {
            FlowManager.gameFlow = GameFlow.GAMEOVER;
        }
    }

    public bool IsDamaged()
    {
        return actionState == ActionState.DAMAGE;
    }

    public bool IsJumping()
    {
        return actionState == ActionState.JUMP;
    }

    /// <summary>
    /// ダメージ中と隠れている最中は動けない
    /// </summary>
    /// <returns></returns>
    public bool CanMove()
    {
        return !IsDamaged();
    }

    /// <summary>
    /// 移動可能かつジャンプ中でなければジャンプ可
    /// </summary>
    /// <returns></returns>
    public bool CanJump()
    {
        return CanMove() && !IsJumping();
    }
}

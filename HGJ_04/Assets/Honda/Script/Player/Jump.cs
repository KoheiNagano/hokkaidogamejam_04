﻿using UnityEngine;
using System.Collections;

public class Jump : MonoBehaviour {
    private State state;
    private Rigidbody2D rigid;

    // Use this for initialization
    void Start ()
    {
        state = GetComponent<State>();
        rigid = GetComponent<Rigidbody2D>();
    }
	
	// Update is called once per frame
	void Update () {
        Jumping();
    }

    void Jumping()
    {
        if (!state.CanJump()) return;

        if (Input.GetButtonDown("Jump"))
        {
            rigid.AddForce(new Vector2(0, 100));
            State.actionState = ActionState.JUMP;
        }
    }
}

﻿using UnityEngine;
using System.Collections;

using HondaUtil;

public class ActionProcess : MonoBehaviour
{
    [SerializeField]
    AreaController controller;

    [SerializeField]
    AudioClip avoid;
    [SerializeField]
    AudioSource se;

    Timer timer;

    void OnEnable()
    {
        Debug.Log("aaaaaa");
        controller.SetNextArea(controller.gimmickNames[Random.Range(0, controller.gimmickNames.Count)]);
    }

    // Use this for initialization
    void Start()
    {
        timer = new Timer(5);
    }

    // Update is called once per frame
    void Update()
    {
        if (FlowManager.gameFlow != GameFlow.ACTION) return;

        timer.Update();

        if (timer.IsEnd())
        {
            se.clip = avoid;
            se.Play();
            FlowManager.avoidCount++;
            timer.Initialize();
            FlowManager.gameFlow = GameFlow.COMMAND;
        }
    }
}

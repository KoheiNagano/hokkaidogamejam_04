﻿using UnityEngine;
using System.Collections;

using UnityEngine.SceneManagement;
using System.Collections.Generic;

public enum GameFlow
{
    TITLE,
    COMMAND,
    ACTION,
    GAMEOVER,
}

public class FlowManager : MonoBehaviour
{
    public static GameFlow gameFlow;
    public static int score;
    public static int avoidCount;

    [SerializeField]
    GameObject GameOverMgr;
    [SerializeField]
    ActionProcess action;

    public static AudioSource audioSource;

    // Use this for initialization
    void Start()
    {
        avoidCount = 0;
        gameFlow = GameFlow.TITLE;
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log("avoid = " + avoidCount);
        Debug.Log("flow = " + gameFlow);
        SceneFlow();
    }

    //シーンのような動きをする何か
    void SceneFlow()
    {
        switch (gameFlow)
        {
            case GameFlow.TITLE:
                Title();
                break;

            case GameFlow.GAMEOVER:
                GameOver();
                break;

            case GameFlow.COMMAND:
                action.enabled = false;
                break;

            case GameFlow.ACTION:
                ActionStart();
                break;

            default:
                break;
        }
    }

    void ActionStart()
    {
        action.enabled = true;
    }

    void Title()
    {
        //仮設置・スペースキーで開始
        if (Input.GetButtonDown("Jump"))
        {
            GameObject.Find("Title").GetComponent<TitleDirector>().enabled = true;
        }
    }

    void GameOver()
    {
        GameOverMgr.SetActive(true);
        if (Input.GetButtonDown("Jump"))
        {
            SceneManager.LoadScene("Main");
        }
    }
}


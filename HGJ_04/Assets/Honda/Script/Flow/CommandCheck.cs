﻿using UnityEngine;
using System.Collections;

using HondaUtil;
using System.Collections.Generic;

 enum DirectionPad
{
    NONE=-1,
    RIGHT=0,
    LEFT,
    UP,
    DOWN,
}

public class CommandCheck : MonoBehaviour
{
    readonly static DirectionPad[] direction = new DirectionPad[]
    {
        DirectionPad.RIGHT,
        DirectionPad.LEFT,
        DirectionPad.UP,
        DirectionPad.DOWN,
    };

    //コマンド入力する回数
    [SerializeField]
    int checkCount = 3;
    //コマンドの数
    public int commandCount = 3;
    //入力待ち時間
    [SerializeField]
    int waitTime = 2;
    //十字キースプライト・順番注意(enumに合わせる)
    [SerializeField]
    GameObject[] sprites;

    [SerializeField]
    AudioClip correct;
    [SerializeField]
    AudioClip success;
    [SerializeField]
    AudioClip miss;
    [SerializeField]
    AudioSource se;

    //ランダムで選出されたコマンドのリスト
    DirectionPad[] commandList;
    //計測時間
    Timer timer;
    //プレイヤー情報
    Player player;
    //現在のコマンド位置
    int currentCommand;
    //現在の入力回数
    int currentCheckCount;
    
    bool canInput;
    List<GameObject> spriteInstances;

    Vector3[][] position;
    Timer endDelay;

    void Awake()
    {
        player = GameObject.FindWithTag("Player").GetComponent<Player>();
    }

    // Use this for initialization
    void Start()
    {
        commandList = new DirectionPad[5];
        timer = new Timer(waitTime);
        currentCheckCount = 0;
        currentCommand = 0;
        canInput = true;
        spriteInstances = new List<GameObject>();

        position = new Vector3[3][] 
        {
            new Vector3[3] {new Vector3(-3.46f, 3.35f), new Vector3(0, 3.35f), new Vector3(3.46f, 3.35f)},
            new Vector3[4] {new Vector3(-5, 3.35f), new Vector3(-2, 3.35f), new Vector3(2, 3.35f), new Vector3(5, 3.35f)},
            new Vector3[5] {new Vector3(-6, 3.35f), new Vector3(-3, 3.35f), new Vector3(0, 3.35f), new Vector3(3, 3.35f), new Vector3(6, 3.35f) },
        };
    }

    // Update is called once per frame
    void Update()
    {
        if (FlowManager.gameFlow != GameFlow.COMMAND) return;

        //二重入力防止
        if(Input.GetAxis("Horizontal") == 0 && Input.GetAxis("Vertical") == 0)
        {
            canInput = true;
        }

        //終了条件の確認・終了していたらリターン
        if (EndCheck()) return;

        Debug.Log("Command = " + commandList[currentCommand]);

        FlowManager.score++;

        //コマンド決定・一回のみ
        DecideCommand();

        //タイマー加算
        timer.Update();

        //入力チェック
        InputCheck();

        //タイムアップ時
        TimeUp();
    }

    private void InputCheck()
    {
        if (!canInput) return;

        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");
        Debug.Log("h = " + h);
        Debug.Log("v = " + v);

        DirectionPad selectDir;
        if (h >= 1)
            selectDir = DirectionPad.RIGHT;
        else if (h <= -1)
            selectDir = DirectionPad.LEFT;
        else if (v >= 1)
            selectDir = DirectionPad.UP;
        else if (v <= -1)
            selectDir = DirectionPad.DOWN;
        else
            selectDir = DirectionPad.NONE;

        if (selectDir == DirectionPad.NONE) return;

        //入力チェック
        //正解の場合
        if (selectDir == commandList[currentCommand])
        {
            Destroy(spriteInstances[currentCommand]);
            //コマンドを１進める
            currentCommand += 1;
            canInput = false;
            //コマンド入力成功時処理(規定回数クリアで処理される)
            CommandClear();
            Debug.Log("Success");
        }
        //失敗の場合
        else
        {
            se.clip = miss;
            se.Play();
            canInput = false;
            player.Damage();
            AdvanceProcess();
            Debug.Log("missed");
        }
    }

    private void CommandClear()
    {
        //コマンド正解数が規定数を超えたら
        if (currentCommand >= commandCount)
        {
            se.clip = success;
            se.Play();

            //プレイヤー回復
            player.Recovery();

            AdvanceProcess();

            Debug.Log("Clear");
        }
        else
        {
            se.clip = correct;
            se.Play();
        }
    }

    bool EndCheck()
    {
        if (currentCheckCount >= checkCount)
        {
            FlowManager.gameFlow = GameFlow.ACTION;
            timer.Initialize();
            currentCheckCount = 0;
            currentCommand = 0;

            if (FlowManager.avoidCount % 2 == 1 && commandCount != 5)
            {
                commandCount++;
            }
            return true;
        }

        return false;
    }

    //タイムアップ時処理
    private void TimeUp()
    {
        if (timer.IsEnd())
        {
            //プレイヤーにダメージ
            player.Damage();
            AdvanceProcess();
        }
    }

    //フロー進行
    void AdvanceProcess()
    {
        //タイマー初期化
        timer.Initialize();
        //コマンドの正解数を０に
        currentCommand = 0;
        //入力回数を進める
        currentCheckCount++;

        foreach(GameObject obj in spriteInstances)
        {
            Destroy(obj);
        }
        spriteInstances.Clear();
    }

    //ランダムにコマンドを決定
    void DecideCommand()
    {
        //タイマーが０秒の時＝処理の最初だけ機能する
        if (timer.GetCurrentTime() != 0) return;

        for (int i = 0; i < commandCount; i++)
        {
            commandList[i] = direction[Random.Range(0, direction.Length)];
        }

        for(int i = 0;i < commandCount; i++)
        {
            GameObject s = Instantiate(sprites[(int)commandList[i]]);
            s.transform.position = position[commandCount - 3][i];
            spriteInstances.Add(s);
        }
    }
}

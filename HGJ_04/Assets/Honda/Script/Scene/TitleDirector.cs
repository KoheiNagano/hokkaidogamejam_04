﻿using UnityEngine;
using System.Collections;

using HondaUtil;
using UnityEngine.UI;

public class TitleDirector : MonoBehaviour
{
    [SerializeField]
    Image rogo;
    [SerializeField]
    Image coffinOpen;
    [SerializeField]
    Image start;
    [SerializeField]
    GameObject boss;
    [SerializeField]
    GameObject wall;
    [SerializeField]
    AudioClip main;

    Timer timer;
    public GameObject initFloor;
    public GameObject areaController;

    // Use this for initialization
    void Start()
    {
        timer = new Timer(13);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            timer.TimerForcedEnd();
        }

        int t = timer.GetCurrentTime();

        if (t == 0)
        {
            rogo.enabled = false;
            start.gameObject.SetActive(false);
            coffinOpen.enabled = true;
            boss.GetComponent<MovetoTarget>().SetParameters(boss.transform.position, new Vector3(-2.1f, -2.21f, 0), 2);
            boss.GetComponent<MovetoTarget>().MoveStart();
        }

        if(t == 180)
        {
            coffinOpen.gameObject.SetActive(true);
        }

        if(t >= 300 && t <= 540)
        {
            int i = t % 6 < 3 ? -1 : 1;
            boss.transform.position += new Vector3(0.2f, 0, 0) * i;
        }

        if(t >= 420 && t <= 540)
        {
            boss.transform.localScale += new Vector3(0.02f, 0.02f, 0);
            boss.transform.position += new Vector3(0, 0.02f, 0);
        }

        if(t == 600)
        {
            boss.GetComponent<MovetoTarget>().SetParameters(boss.transform.position, new Vector3(13, -2.21f, 0), 2);
            boss.GetComponent<MovetoTarget>().MoveStart();
        }

        if(t == 720)
        {
            wall.GetComponent<MovetoTarget>().SetParameters(wall.transform.position, new Vector3(-18.53f, 0, 0), 2);
            wall.GetComponent<MovetoTarget>().MoveStart();
        }

        timer.Update();

       // if(t == )

        if (timer.IsEnd())
        {
            FlowManager.gameFlow = GameFlow.COMMAND;
            GameObject.Find("GameMain").GetComponent<GameMain>().enabled = true;
            gameObject.SetActive(false);
            Instantiate(initFloor);
            areaController.GetComponent<AreaController>().RunArea();
            boss.SetActive(false);

            FlowManager.audioSource.clip = main;
            FlowManager.audioSource.Play();
        }
    }
}

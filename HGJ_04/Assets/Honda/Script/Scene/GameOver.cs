﻿using UnityEngine;
using System.Collections;

using UnityEngine.UI;

public class GameOver : MonoBehaviour
{
    [SerializeField]
    Text scoreText;

    // Use this for initialization
    void Start()
    {
        GetComponent<MovetoTarget>().SetParameters(transform.position, new Vector3(0, 0, 0), 0.5f);
        GetComponent<MovetoTarget>().MoveStart();

    }

    // Update is called once per frame
    void Update()
    {
        if(transform.position.y == 0)
        {
            scoreText.text = "Score : " + FlowManager.score + "m";
        }
    }
}

﻿using UnityEngine;
using System.Collections;

public class GameMain : MonoBehaviour
{
    [SerializeField]
    GameObject player;
    [SerializeField]
    GameObject floor;
    [SerializeField]
    GameObject boss;

    // Use this for initialization
    void Start()
    {
        boss.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        player.SetActive(true);
        floor.SetActive(true);
        boss.SetActive(true);
    }
}

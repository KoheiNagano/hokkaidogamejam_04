﻿using UnityEngine;
using System.Collections;

/// <summary>
/// startPositionからendPositionへtime秒で移動させるスクリプト
/// SetParametersで値設定、MoveStartで移動開始
/// </summary>
public class MovetoTarget : MonoBehaviour 
{
    //移動時間
    private float time = 1;
    //開始座標
    private Vector3 startPosition;
    //終点座標
    Vector3 endPosition;
    //開始時間
    private float startTime;
    //処理中か
    private bool isEnabled;

	// Use this for initialization
	void Start () {
        //処理していない
        isEnabled = false;
    }

    /// <summary>
    /// パラメータを設定
    /// </summary>
    /// <param name="startPosition">開始座標</param>
    /// <param name="endPosition">終点座標</param>
    /// <param name="time">移動時間</param>
    public void SetParameters(Vector3 startPosition, Vector3 endPosition, float time)
    {
        //タイムが0以下の場合のエラー対応
        if (!TimeCheck(time)) return;

        //処理の開始時間を記録
        startTime = Time.realtimeSinceStartup;
        //開始座標設定
        this.startPosition = startPosition;
        //終点座標設定
        this.endPosition = endPosition;
        //移動時間設定
        this.time = time;

    }

    /// <summary>
    /// 移動開始
    /// </summary>
    public void MoveStart()
    {
        //処理中に設定
        isEnabled = true;
    }
	
	// Update is called once per frame
	void Update () {
        //処理フラグが立っていなければリターン
        if (!isEnabled) return;

        //経過時間を算出
        var diff = Time.realtimeSinceStartup - startTime;

        //終了判定
        EndCheck(diff);

        //座標移動
        Move(diff);
	}

    /// <summary>
    /// 座標移動
    /// </summary>
    /// <param name="diff">経過時間</param>
    private void Move(float diff)
    {
        //現在の進行割合を算出
        var rate = diff / time;
        //進行割合を元に座標移動
        transform.position = Vector3.Lerp(startPosition, endPosition, rate);
    }

    /// <summary>
    /// 終了判定
    /// </summary>
    /// <param name="diff">経過時間</param>
    private void EndCheck(float diff)
    {
        //経過時間が指定した時間を過ぎていたら
        if (diff > time)
        {
            transform.position = endPosition;//終点座標に固定して
            isEnabled = false;//処理終了
        }
    }


    /// <summary>
    /// timeが０より大きいかチェック
    /// </summary>
    /// <param name="time">移動時間</param>
    /// <returns>０より大きいかどうか</returns>
    private bool TimeCheck(float time)
    {
        if (time <= 0)
        {
            transform.position = endPosition;
            isEnabled = false;
            Debug.Log("timeが０以下です");
            return false;
        }
        return true;
    }

    /// <summary>
    /// 処理中かどうか
    /// </summary>
    /// <returns></returns>
    public bool IsEnabled()
    {
        return isEnabled;
    }

    /// <summary>
    /// 移動を強制終了
    /// </summary>
    public void MoveStop()
    {
        isEnabled = false;
     //   endPosition = transform.position;
    }
}

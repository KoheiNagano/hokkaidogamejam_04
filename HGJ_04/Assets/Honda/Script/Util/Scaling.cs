﻿using UnityEngine;
using System.Collections;

public class Scaling : MonoBehaviour
{
    [SerializeField]
    float rate = 1.2f;
    [SerializeField]
    float scalingSpeed = 0.2f;

    float timer;

    // Use this for initialization
    void Start()
    {
        timer = 0;
    }

    // Update is called once per frame
    void Update()
    {
        timer = timer > 180 ? 0 : timer + scalingSpeed;
        
        transform.localScale = (transform.localScale * (1 + ((rate - 1) * Mathf.Sin(timer))));
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HondaUtil
{
    /// <summary>
    /// 加算式ストップウォッチ型タイマー
    /// </summary>
    class Timer
    {
        protected int time;
        protected Range<int> range;

        protected const int ToSecond = 60;

        /// <summary>
        /// デフォルトコンストラクタ・１秒に設定
        /// </summary>
        public Timer()
        {
            range = new Range<int>(0, 1 * ToSecond);
            Initialize(1);
        }

        /// <summary>
        /// コンストラクタ・計測する秒数を指定
        /// </summary>
        /// <param name="time">計測する秒数</param>
        public Timer(float limitTime)
        {
            range = new Range<int>(0, (int)(limitTime * ToSecond));
            Initialize(limitTime);
        }

        /// <summary>
        /// タイマー初期化
        /// </summary>
        public void Initialize()
        {
            time = 0;
        }

        /// <summary>
        /// タイマー初期化・制限時間再設定
        /// </summary>
        /// <param name="limitTime"></param>
        public virtual void Initialize(float limitTime)
        {
            time = 0;
            range.SetMax((int)(limitTime * ToSecond));
        }

        /// <summary>
        /// 更新処理
        /// </summary>
        public virtual void Update()
        {
            range.Clamp(++time);
        }

        /// <summary>
        /// タイマーの終了判定
        /// </summary>
        /// <returns>終了=true</returns>
        public virtual bool IsEnd()
        {
            return range.GreaterThanMax(time);
        }

        /// <summary>
        /// 終了までの割合
        /// </summary>
        /// <returns></returns>
        public float Rate()
        {
            return 1.0f - (time / range.GetMax());
        }

        /// <summary>
        /// num秒おきにtrue
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
        public bool MultiplyOf(int num)
        {
            return (time % (num * ToSecond) == 0);
        }

        /// <summary>
        /// 新規追加・現在時間取得メソッド
        /// </summary>
        /// <returns></returns>
        public int GetCurrentTime()
        {
            return time;
        }

        /// <summary>
        /// タイマーを強制終了させる
        /// </summary>
        public virtual void TimerForcedEnd()
        {
            time = range.GetMax();
        }
    }
}

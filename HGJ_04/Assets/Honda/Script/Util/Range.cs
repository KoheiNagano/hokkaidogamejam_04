﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Diagnostics;

namespace HondaUtil
{
    class Range<T>
        where T : IComparable
    {
        private T min;
        private T max;

        public Range(T min, T max)
        {
            Debug.Assert(min.CompareTo(max) < 0, "Rangeに設定した最小数が最大数より大きいです");
            this.min = min;
            this.max = max;
        }

        public bool GreaterThanMax(T value)
        {
            return max.CompareTo(value) < 0;
        }

        public bool LessThanMin(T value)
        {
            return min.CompareTo(value) > 0;
        }

        public bool IsInside(T value)
        {
            return !GreaterThanMax(value) && !LessThanMin(value);
        }

        public bool IsMax(T value)
        {
            return max.Equals(value);
        }

        public bool IsMin(T value)
        {
            return min.Equals(value);
        }

        public T Clamp(T value)
        {
            if (GreaterThanMax(value)) return max;
            if (LessThanMin(value)) return min;
            return value;
        }

        public T Wrap(T value)
        {
            if (GreaterThanMax(value)) return min;
            if (LessThanMin(value)) return max;
            return value;
        }

        public T GetMin()
        {
            return min;
        }

        public T GetMax()
        {
            return max;
        }

        public void SetMin(T min)
        {
            this.min = min;
        }

        public void SetMax(T max)
        {
            this.max = max;
        }
    }
}

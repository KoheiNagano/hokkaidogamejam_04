﻿using UnityEngine;
using System.Collections;

using UnityEngine.UI;
using HondaUtil;

public class Flashing : MonoBehaviour
{
    [SerializeField]
    float rate = 0.5f;

    private Timer timer;
    private Image image;

    // Use this for initialization
    void Start()
    {
        timer = new Timer(rate);
        image = GetComponent<Image>();
       // enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        timer.Update();

        if(timer.IsEnd())
        {
            image.enabled = !image.enabled;
            timer.Initialize();
        }
    }

    void OnDisable()
    {
        if (image == null) return;
        image.enabled = false;
    }
}
